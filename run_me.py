#!/usr/bin/env python3
import time
INTERVAL = 1

print("This program is running as %s" % __name__)

time.sleep(INTERVAL)
print("I'm going to import 'foo' now")
import foo

time.sleep(INTERVAL)
print("I'm going to call some_func from library 'foo'")
foo.some_func()


time.sleep(INTERVAL)
print("I'm going to import library 'bar' now")
import bar
time.sleep(INTERVAL)
